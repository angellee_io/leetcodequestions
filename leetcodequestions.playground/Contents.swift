//: Playground - noun: a place where people can play

import UIKit

class Solution {
    func twoSum(_ nums: [Int], _ target: Int) -> [Int] {
        var dict = [Int: Int]();
        var result = [Int]();
        var difference = 0
        
        for (index, num) in nums.enumerated() {
            difference = target - num
            
            if dict.keys.contains(num){
                let indexTwo = dict[num]
                result = [index, indexTwo!]
                break
            }
            
            if dict[difference] == nil {
                dict[difference] = index
            }
        }
        return result
    }
}



